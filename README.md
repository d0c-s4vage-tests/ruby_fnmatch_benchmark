This project is a quick benchmark of various methods that can perform
fnmatch-type matching on a list of paths:

```
$> ruby -v
ruby 2.6.5p114 (2019-10-01 revision 67812) [x86_64-linux]
$> ruby test.rb 1000 10_000 "*/*a*"
---------------------------------
Benchmarking fnmatch methods with
        iters: 1000
       checks: 10000
      pattern: "*/*a*"
regex pattern: /.*\/.*a.*/
---------------------------------
                                     user     system      total        real
File.fnmatch?                    4.626635   0.000000   4.626635 (  4.626791)
                                                                              answers:     0 (0.00%) extra     0 (0.00%) missing
(pattern->Regexp).match?         2.635695   0.000000   2.635695 (  2.635773)
                                                                              answers:   123 (0.29%) extra     0 (0.00%) missing
```
