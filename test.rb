require 'benchmark'
require 'set'

def ruby_fnmatch(path, pattern)
  File.fnmatch?(pattern, path, File::FNM_PATHNAME | File::FNM_DOTMATCH | File::FNM_EXTGLOB)
end

def regex_fnmatch(path, pattern)
  pattern.match?(path)
end

# from https://stackoverflow.com/questions/11276909/how-to-convert-between-a-glob-pattern-and-a-regexp-pattern-in-ruby
def convert_pattern_to_regex(pattern)
  chars = pattern.split('')
  in_curlies = 0
  escaping = false
  chars.map do |char|
    if escaping
      escaping = false
      char
    else
      case char
        when '*'
          ".*"
        when "?"
          "."
        when "."
          "\\."

        when "{"
          in_curlies += 1
          "(?:"
        when "}"
          if in_curlies > 0
            in_curlies -= 1
            ")"
          else
            char
          end
        when ","
          if in_curlies > 0
            "|"
          else
            char
          end
        when "\\"
          escaping = true
          "\\"

        else
          char

      end
    end
  end.join
end

def stats(test_answers, real_answers)
  missing = (real_answers - test_answers).length
  missing_pct = (missing / real_answers.length.to_f).round(2)
  extra = (test_answers - real_answers).length
  extra_pct = (extra / real_answers.length.to_f).round(2)

  'answers: %5d (%3.02f%%) extra %5d (%3.02f%%) missing' % [
    extra,
    extra_pct,
    missing,
    missing_pct
  ]
end

if ARGV.length < 3
  puts "USAGE: #{__FILE__} N_ITERS N_CHECKS GLOB"
  exit 1
end

num_iters = ARGV[0].to_i
num_checks = ARGV[1].to_i
pattern = ARGV[2]
regex_pattern = Regexp.new(convert_pattern_to_regex(pattern))

puts '---------------------------------'
puts 'Benchmarking fnmatch methods with'
puts "        iters: #{num_iters}"
puts "       checks: #{num_checks}"
puts "      pattern: #{pattern.inspect}"
puts "regex pattern: #{regex_pattern.inspect}"
puts '---------------------------------'

# list of all files is created using:
#
#   pipeline.all_worktree_paths # lib/gitlab/ci/build/rules/rule/clause/exists.rb
#
# which uses:
#
#   project.repository.ls_files(sha) # app/models/ci/pipeline.rb
#
# which uses:
#
#   gitaly_repository_client.ls_files(ref) # lib/gitlab/git/repository.rb
#
# This could actually be the bottleneck - doing a Gitaly call to fetch the list
# of files may be more problematic than the actual comparisons
#
# File.fnmatch? function is defined here: https://github.com/ruby/ruby/blob/1020e120e060c00eca456d5a129c344daa472407/dir.c#L3266

charset = ('/'..'~').to_a
checks = (0..num_checks).map do
  (0..30).map { charset[rand(charset.length)] }.join
end

benchmarks = [
  ['File.fnmatch?', pattern, :ruby_fnmatch],
  ['(pattern->Regexp).match?', regex_pattern, :regex_fnmatch]
]

answers = Set.new(checks.filter {|path| ruby_fnmatch(path, pattern) })

Benchmark.bm(30) do |bm|
  benchmarks.each do |bench_name, bench_pattern, bench_fn|
    bench_fn = method(bench_fn)

    bm.report(bench_name) do
      num_iters.times do
        for path in checks do
          bench_fn.call(path, bench_pattern)
        end
      end
    end

    bench_fn_answers = Set.new(checks.filter { |path| bench_fn.call(path, bench_pattern) })
    padding = ' ' * 78
    puts "#{padding}#{stats(bench_fn_answers, answers)}"
  end
end
